# Social Network Registration Form

## Functional Requirements
1. The user should be able to enter a username.
2. The username field should be mandatory (required).
3. The user should be able to enter their email address.
4. The email field should be mandatory (required).
5. The user should be able to enter a password.
6. The password field should be mandatory (required).
7. The user should be able to confirm their password.
8. The confirm password field should be mandatory (required).
9. The passwords entered in both fields (password and confirm password) should match.
10. Upon successful registration the system should acknowledge the submission displaying a success message.

## Non-Functional Requirements
1. The registration form should be responsive and display well on different devices (desktop, mobile, tablets).
2. The form should be user-friendly and easy to understand.
3. The page should load quickly and perform well.
4. Error messages should be clear and informative.
